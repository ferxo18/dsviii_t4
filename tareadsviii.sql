create database oferta

create table factura(
numfact int not null,
cod varchar(15) not null,
descripcion varchar(30) not null,
precio float not null,
descuento float default 0.10,
comision float default 0.0150

)

insert into factura values(1,'27chd-mf','Bateria Autocraft',118.81,0.10,0.0150)
insert into factura values(2,'smf-nx120-7','Bateria Global',188.32,0.10,0.0150)
insert into factura values(3,'N70ZR','Bateria record',95.54,0.10,0.0150)
insert into factura values(4,'27950','Bateria Mac',153.95,0.10,0.0150)
insert into factura values(5,'VW12R-MF','Bateria Record',59.99,0.10,0.0150)